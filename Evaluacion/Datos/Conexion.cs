﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Datos
{
    public class Conexion
    {
        SqlConnection cn = new SqlConnection("Data Source=GRYFFINDOR-PC;Initial Catalog=Evaluacion;Integrated Security=True");

        public SqlDataReader lector;
        public DataTable tabla = new DataTable();
        public SqlCommand consulta = new SqlCommand();
        protected string sql;


        public void conectar()
        {

            try
            {

                consulta.Connection = cn;
                cn.Open();
            }

            catch
            {

                MessageBox.Show("No se puede conectar con la base de datos comniquese con su administrador", "Conexion Fallida", MessageBoxButtons.RetryCancel, MessageBoxIcon.Error);

                desconectar();
                throw;
            }
        }

        private void desconectar()
        {
            cn.Close();
        }

        public void ejecutar_sentencias_sql(string sentencia)
        {
            try
            {
                consulta.CommandText = sentencia;
                conectar();
                consulta.ExecuteNonQuery();
                desconectar();
            }
            catch (Exception ex)
            {

                MessageBox.Show("No se puede ejecutar la sentencia de SQL" + ex + "", "Error Sentencia SQL", MessageBoxButtons.OK, MessageBoxIcon.Error);
                desconectar();
            }
        }

        public DataTable ejecutar_consultas_sql(string sentencia)
        {

            tabla.Clear();

            try
            {

                consulta.CommandText = sentencia;
                conectar();
                lector = consulta.ExecuteReader();
                tabla.Load(lector);
                lector.Close();

                desconectar();
            }

            catch
            {
                MessageBox.Show("No se ha podido realizar la consulta de SQL", "Error Consulta de SQL", MessageBoxButtons.OK, MessageBoxIcon.Error);
                lector.Close();

                desconectar();
            }

            return tabla;
        }

        public void llenaritems(ComboBox cmb)
        {
            cn.Open();
            try
            {
                SqlCommand cmd = new SqlCommand("Select usuario From usuario", cn);
                lector = cmd.ExecuteReader();
                while (lector.Read())
                {
                    cmb.Items.Add(lector["usuario"].ToString());

                }
                lector.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show("Algo anda mal" + ex.ToString());
            }
            cn.Close();
        }


        public void llenaritems1(ComboBox cmb)
        {
            cn.Open();
            try
            {
                SqlCommand cmd = new SqlCommand("Select cargo From usuario", cn);
                lector = cmd.ExecuteReader();
                while (lector.Read())
                {
                    cmb.Items.Add(lector["cargo"].ToString());

                }
                lector.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show("Algo anda mal" + ex.ToString());
            }
            cn.Close();
        }


        public void llenaritems2(ComboBox cmb)
        {
            cn.Open();
            try
            {
                SqlCommand cmd = new SqlCommand("Select estado From usuario", cn);
                lector = cmd.ExecuteReader();
                while (lector.Read())
                {
                    cmb.Items.Add(lector["estado"].ToString());

                }
                lector.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show("Algo anda mal" + ex.ToString());
            }
            cn.Close();
        }
    }
}
