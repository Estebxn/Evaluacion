﻿using Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Producto
    {
        Int64 idProducto;
        string Descripcion;
        string Departamento;
        double IVA;
        double Precio;
        string Notas;
        Int32 Medidas;


        public long IdProducto
        {
            get
            {
                return idProducto;
            }

            set
            {
                idProducto = value;
            }
        }

        public string Descripcion1
        {
            get
            {
                return Descripcion;
            }

            set
            {
                Descripcion = value;
            }
        }

        public string Departamento1
        {
            get
            {
                return Departamento;
            }

            set
            {
                Departamento = value;
            }
        }

        public double IVA1
        {
            get
            {
                return IVA;
            }

            set
            {
                IVA = value;
            }
        }

        public double Precio1
        {
            get
            {
                return Precio;
            }

            set
            {
                Precio = value;
            }
        }

        public string Notas1
        {
            get
            {
                return Notas;
            }

            set
            {
                Notas = value;
            }
        }

        public Int32 Medidas1
        {
            get
            {
                return Medidas;
            }

            set
            {
                Medidas = value;
            }
        }

        string sentencia;
        Conexion oConex = new Conexion();

        public void AgregarProducto(Int64 idProducto, string Descripcion, string Pepartamento, double IVA, double Precio, string Notas, Int32 Medida)
        {
            sentencia = "exec AgregarValores " + idProducto + ", '" + Descripcion + "', '" + Departamento + "'," + IVA + ", " + Precio + ", '" + Notas + "', " + Medida + "";
            oConex.ejecutar_sentencias_sql(sentencia);
        }

        public void ActualizarProducto(Int64 idProducto, string Descripcion, string Pepartamento, double IVA, double Precio, string Notas, Int32 Medida)
        {
            sentencia = "exec ActualizarValores " + idProducto + ", '" + Descripcion + "', '" + Departamento + "'," + IVA + ", " + Precio + ", '" + Notas + "', " + Medida + "";
            oConex.ejecutar_sentencias_sql(sentencia);
        }

        public void EliminarProductos(Int64 IdProducto)
        {
            sentencia = "exec EliminarProductos " + idProducto + "";
            oConex.ejecutar_sentencias_sql(sentencia);
        } 
    }
}
