﻿using Logica;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        Producto oProd = new Producto();

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            oProd.IdProducto = Int64.Parse(txtIdProduc.Text);
            oProd.Descripcion1 = rtbDescrip.Text;
            oProd.Departamento1 = txtDepartamento.Text;
            oProd.IVA1 = double.Parse(txtIVA.Text);
            oProd.Precio1 = double.Parse(txtPrecio.Text);
            oProd.Notas1 = rtbNotas.Text;
            oProd.Medidas1 = Int32.Parse(txtMedida.Text);

            oProd.AgregarProducto(Int64.Parse(txtIdProduc.Text), rtbDescrip.Text, txtDepartamento.Text, double.Parse(txtIVA.Text), double.Parse(txtPrecio.Text), rtbNotas.Text, Int32.Parse(txtMedida.Text));
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            oProd.IdProducto = Int64.Parse(txtIdProduc.Text);
            oProd.Descripcion1 = rtbDescrip.Text;
            oProd.Departamento1 = txtDepartamento.Text;
            oProd.IVA1 = double.Parse(txtIVA.Text);
            oProd.Precio1 = double.Parse(txtPrecio.Text);
            oProd.Notas1 = rtbNotas.Text;
            oProd.Medidas1 = Int32.Parse(txtMedida.Text);

            oProd.ActualizarProducto(Int64.Parse(txtIdProduc.Text), rtbDescrip.Text, txtDepartamento.Text, double.Parse(txtIVA.Text), double.Parse(txtPrecio.Text), rtbNotas.Text, Int32.Parse(txtMedida.Text));
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            oProd.IdProducto = Int64.Parse(txtIdProduc.Text);

            oProd.EliminarProductos(Int64.Parse(txtIdProduc.Text));
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
